import 'package:berasku/Model/M_beranda.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import '../V_product_detail.dart';

class WBeranda {
  MBeranda beranda = MBeranda();

  swiper() {
    return Container(
      height: 150.0,
      color: Color(0xffea2c2c),
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return Image.asset(
            beranda.listImg[index],
            fit: BoxFit.cover,
          );
        },
        itemCount: beranda.listImg.length,
        pagination: SwiperPagination(alignment: Alignment.bottomLeft),
        autoplay: true,
        autoplayDelay: 6000,
      ),
    );
  }

  kategori(String title, String soon, Color _front) {
    return Container(
      child: Column(
        children: <Widget>[
          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                height: 60.0,
                width: 60.0,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/img/icon.png"))),
              ),
              Container(
                height: 60.0,
                width: 60.0,
                color: _front,
              ),
              Positioned(
                  top: -8.0,
                  right: -10.0,
                  child: Text(soon,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.red)))
            ],
          ),
          SizedBox(height: 5.0),
          Text(title)
        ],
      ),
    );
  }

  laris(
    String title,
    String price,
    String tag,
    BuildContext context,
    String img,
  ) {
    return Padding(
      padding: const EdgeInsets.only(right: 28),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (c) => VProductDetail(
                tag: tag,
                img: img,
                title: title,
                harga: price,
              ),
            ),
          );
        },
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Hero(
                tag: tag,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                      image: NetworkImage(img),
                      fit: BoxFit.cover,
                    ),
                  ),
                  // color: Colors.blue,
                  height: 120,
                  width: 120,
                ),
              ),
              SizedBox(height: 5.0),
              Text(title),
              SizedBox(height: 5.0),
              Text("Rp. $price", style: TextStyle(color: Colors.red))
            ],
          ),
        ),
      ),
    );
  }

  all(
    String title,
    String price,
    String tag,
    BuildContext context,
    String img,
  ) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Container(
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (c) => VProductDetail(
                tag: tag,
                img: img,
                title: title,
                harga: price,
              ),
            ),
          );
        },
        child: Container(
          // height: 200,
          child: Column(
            // physics: NeverScrollableScrollPhysics(),
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: NetworkImage(img),
                    fit: BoxFit.cover,
                  ),
                ),
                height: queryData.devicePixelRatio >= 2.0 ? 120 : 170,
                width: MediaQuery.of(context).size.width / 2 - 20,
              ),
              SizedBox(height: 5.0),
              Text(
                title,
                style: TextStyle(fontWeight: FontWeight.w700),
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 5.0),
              Text("Rp. $price", style: TextStyle(color: Colors.red))
            ],
          ),
        ),
      ),
    );
  }
}
