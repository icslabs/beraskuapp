import 'package:berasku/Controller/C_auth.dart';
import 'package:berasku/View/V_home.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

// HANDLE IMAGE PROFILE
class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: -50.0,
      child: Container(
        child: CircleAvatar(
          backgroundColor: Colors.white,
          radius: 110.0,
          child: CircleAvatar(
            backgroundColor: Theme.of(context).primaryColor,
            radius: 100.0,
            backgroundImage: AssetImage("assets/img/icon.png"),
          ),
        ),
      ),
    );
  }
}

// HANDLE TITLE
class TitleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 165.0,
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Text(
          "Berasku",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40.0,
              fontFamily: "Lobster",
              color: Theme.of(context).primaryColor),
        ),
      ),
    );
  }
}

Future<SharedPreferences> _sharedPref = SharedPreferences.getInstance();

// HANDLE LOGIN BUTTON
class LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future _handleGet() async {
      final sha = await _sharedPref;
      final data = sha.getString("name");

      if (data == null || data.isEmpty) {
        print("failed");
      } else {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (c) => Home(name: data)));
      }
    }

    return Positioned(
      bottom: 160.0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child: MaterialButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          padding: EdgeInsets.all(12.0),
          minWidth: MediaQuery.of(context).size.width - 120.0,
          onPressed: () {
            _handleGet();
          },
          color: Theme.of(context).primaryColor,
          splashColor: Colors.redAccent,
          highlightColor: Colors.red[100],
          child: Text(
            "Login With Email",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

// HANDLE REGISTER
class RegisterButton extends StatefulWidget {
  RegisterButton({this.name});

  final String name;

  @override
  _RegisterButtonState createState() => _RegisterButtonState();
}

class _RegisterButtonState extends State<RegisterButton> {
  String d;

  @override
  void initState() {
    super.initState();
    print(widget.name);
    d = "";
  }

  Future _handleLogin() async {
    final sha = await _sharedPref;
    sha.setString("name", widget.name);
    return sha;
  }

  Future _handleGet() async {
    final sha = await _sharedPref;
    d = sha.getString("name");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 100.0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child: MaterialButton(
          padding: EdgeInsets.all(12.0),
          minWidth: MediaQuery.of(context).size.width - 120.0,
          onPressed: () {
            _handleLogin();
            _handleGet();
            print(widget.name);
            print(d);
          },
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
              side: BorderSide(
                  color: Theme.of(context).primaryColor, width: 2.0)),
          splashColor: Colors.redAccent,
          highlightColor: Colors.red[100],
          child: Text(
            "Register With Email",
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

// HANDLE BACKGROUND WIDGET FOR ANY WIDGET IN STACK
class BackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(30.0)),
      height: MediaQuery.of(context).size.height / 1.6,
      padding: EdgeInsets.all(30.0),
    );
  }
}

class GoogleLogin extends StatefulWidget {
  @override
  _GoogleLoginState createState() => _GoogleLoginState();
}

class _GoogleLoginState extends State<GoogleLogin> {
  bool _busy = false;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 20,
      child: Container(
          child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(width: 60.0, child: Divider()),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text('or'),
              ),
              SizedBox(width: 60.0, child: Divider()),
            ],
          ),
          SizedBox(height: 8),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Consumer<CAuth>(
              builder: (c, auth, _) => MaterialButton(
                padding: EdgeInsets.all(12.0),
                minWidth: MediaQuery.of(context).size.width - 120.0,
                onPressed: _busy
                    ? null
                    : () {
                        setState(() {
                          _busy = true;
                        });
                        auth.logIn(context);
                        setState(() {
                          _busy = true;
                        });
                      },
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.black, width: 2.0)),
                splashColor: Colors.redAccent,
                highlightColor: Colors.red[100],
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: Image.asset('assets/img/google.png', scale: 2.2),
                    ),
                    Text(
                      "Login With Google",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }
}
