import 'package:berasku/Model/M_wishlist.dart';
import 'package:flutter/material.dart';

class WWishlist extends StatefulWidget {
  @override
  _WWishlistState createState() => _WWishlistState();
}

class _WWishlistState extends State<WWishlist> {
  List listItems = List();

  @override
  void initState() {
    super.initState();
    listItems = MWishlist().mywishlist;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height - 200.0,
      child: GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 30.0,
          mainAxisSpacing: 20.0,
          childAspectRatio: 20.0 / 21.0,
          physics: ScrollPhysics(),
          children: listItems
              .map((i) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        color: Colors.red,
                        height: 98.0,
                        width: MediaQuery.of(context).size.width / 2 - 20,
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(i["title"]),
                              SizedBox(height: 5.0),
                              Text("Rp. " + i["price"].toString(),
                                  style: TextStyle(color: Colors.red))
                            ],
                          ),
                          IconButton(
                            icon: Icon(Icons.delete, color: Colors.red),
                            onPressed: () {
                              // print(myWishlist[i]);
                              print(i);
                              listItems.remove(i);
                              setState(() {});
                              // CWishlist().removeItem(i);
                              // cWishlist.removeItem(i);
                            },
                          )
                        ],
                      )
                    ],
                  ))
              .toList()),
    );
  }
}
