import 'package:flutter/material.dart';

class WMessage {
  item(String title, String subtitle, String desc) {
    return Container(
        padding: EdgeInsets.all(20.0),
        margin: EdgeInsets.only(bottom: 12.0),
        color: Colors.white,
        child: Column(
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              leading:
                  Icon(Icons.account_circle, color: Colors.green, size: 40.0),
              title: Text(title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.red)),
              subtitle: Text(subtitle,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )),
            ),
            SizedBox(height: 20.0),
            Text(desc),
          ],
        ));
  }
}
