import 'package:berasku/Controller/C_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WAccount {
  trans(String code, double price, String status) {
    return Container(
      color: Colors.white,
      child: ListTile(
          leading: Container(
            height: 100.0,
            width: 80.0,
            color: Colors.red,
          ),
          title: Text(code, style: TextStyle(fontWeight: FontWeight.bold)),
          subtitle: Text("Rp. $price",
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
          trailing: Text(status)),
    );
  }

  cashout(
    String date,
    double price,
  ) {
    return Container(
        color: Colors.white,
        child: ListTile(
          leading: Container(
            height: 100.0,
            width: 80.0,
            color: Colors.red,
          ),
          subtitle: Text("Rp. $price",
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
          title: Text(date, style: TextStyle(fontWeight: FontWeight.bold)),
        ));
  }

  profile(
    BuildContext context,
    String email,
    String username,
    String photoUrl,
  ) {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(10.0),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0)),
                color: Colors.transparent,
                shadowColor: Colors.grey[200],
                elevation: 15.0,
                child: CircleAvatar(
                  radius: 55.0,
                  backgroundColor: Colors.white,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(photoUrl),
                    backgroundColor: Colors.amber,
                    radius: 50.0,
                  ),
                )),
            SizedBox(height: 8.0),
            Container(
              width: 160,
              child: Text(
                '$username',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.black87,
                    fontWeight: FontWeight.w700),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              "$email",
              style: TextStyle(
                fontSize: 14,
                color: Colors.red,
                fontFamily: 'Lobster',
              ),
            ),
            Consumer<CAuth>(
              builder: (c, auth, _) => Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  OutlineButton(
                    borderSide: BorderSide(color: Colors.red, width: 2.0),
                    onPressed: () {
                      auth.logOut(context);
                      print('Status : $username');
                    },
                    child: Text(
                      "Logout",
                      style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  ),
                  OutlineButton(
                    borderSide: BorderSide(color: Colors.green, width: 2.0),
                    onPressed: () {
                      // auth.logOut(context);
                      print('Status : $username');
                    },
                    child: Text(
                      "Edit",
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 5.0),
          ],
        ));
  }
}
