import 'package:flutter/material.dart';

class WFeed {
  item(String title, String subtitle, String desc) {
    return Container(
        margin: EdgeInsets.only(bottom: 12.0),
        padding: EdgeInsets.all(20.0),
        color: Colors.white,
        child: Column(
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              leading: Icon(
                Icons.feedback,
                color: Colors.green,
                size: 35.0,
              ),
              title: Text(title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.red)),
              subtitle: Text(subtitle,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )),
            ),
            SizedBox(height: 20.0),
            Text(desc),
          ],
        ));
  }
}
