import 'package:berasku/Controller/C_cart.dart';
import 'package:flutter/material.dart';

class WCart {
  itemCart(String title, double price, int qty) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      color: Colors.white,
      child: ListTile(
        leading: Container(
          height: 100.0,
          width: 80.0,
          color: Colors.red,
        ),
        subtitle: Text("Rp. $price",
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
        title: Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
        trailing: Container(
          // width: 130.0,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.remove),
                onPressed: (){
                  return CCart().decrementQty(title);
                }
              ),
              Text(CCart().qty.toString()),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: (){
                  return CCart().incrementQty(title);
                }
              ),
            ],
          ),
        ),
      ),
    );
  }

  totalItemPrice(double total) {
    return Container(
      color: Colors.white,
      child: ListTile(
        subtitle: Text("Rp. $total",
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
        title: Text("Total Belanja",
            style: TextStyle(fontWeight: FontWeight.bold)),
        trailing: MaterialButton(
          color: Colors.green,
          onPressed: () {},
          padding: EdgeInsets.all(10.0),
          child: Text("Checkout",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
        ),
      ),
    );
  }

  alamat(String lang, String lat) {
    return Container(
      height: 200.0,
      color: Colors.amber,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text("#Google Maps Here"),
          Text("lang : $lang"),
          Text("lat: $lat"),
        ],
      ),
    );
  }
}
