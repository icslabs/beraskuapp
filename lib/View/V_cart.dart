import 'package:berasku/Controller/C_cart.dart';
import 'package:flutter/material.dart';

class VCart extends StatelessWidget {
  final CCart cCart = CCart();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: ListView(
        children: <Widget>[
          Text("Semua Cart"),
          Divider(),
          SizedBox(height: 10.0),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: cCart.handleDataItem()
            ),
          ),
          SizedBox(height: 20.0),
          Text("Alamat Pengantaran"),
          Divider(),
          SizedBox(height: 5.0),
          cCart.handleAlamat(),
          SizedBox(height: 10.0),
          cCart.handleTotalItemsPrice()
        ],
      ),
    );
  }
}
