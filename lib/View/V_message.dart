import 'package:berasku/Controller/C_message.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class VMessage extends StatelessWidget {
  final CMessage cMessage = CMessage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            FontAwesomeIcons.angleLeft,
            color: Colors.black,
          ),
        ),
        title: Text(
          "Message",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: ListView(children: cMessage.handleDataItems()),
      ),
    );
  }
}
