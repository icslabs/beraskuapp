import 'package:berasku/Controller/C_auth.dart';
import 'package:berasku/View/Widget/W_login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VLogin extends StatefulWidget {
  @override
  _VLoginState createState() => _VLoginState();
}

class _VLoginState extends State<VLogin> {
  TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: "Test");
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => CAuth(),
      child: Scaffold(
        backgroundColor: Colors.red,
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(30.0),
          child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: <Widget>[
              BackWidget(),
              Profile(),
              TitleApp(),
              LoginButton(),
              RegisterButton(name: _controller.text),
              GoogleLogin(),
            ],
          ),
        ),
        bottomNavigationBar: Container(
            padding: EdgeInsets.all(20.0),
            child: Text("v1.0",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold))),
      ),
    );
  }
}
