import 'package:flutter/material.dart';

class VAccountEdit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: ListView(
          children: <Widget>[
            CircleAvatar(
              radius: 65.0,
              backgroundColor: Colors.red,
              child: CircleAvatar(
                backgroundColor: Colors.grey[200],
                radius: 60.0,
                child: Center(child: Text("Change photo", style: TextStyle(
                  color: Colors.black87
                ))),
              ),
            ),
            SizedBox(height: 20.0,),
            TextField(
              decoration: InputDecoration(
                  labelText: "No. Handphone",
                  border: OutlineInputBorder(
                      // borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(width: 2.0, color: Colors.grey)),
                  hintText: "No Handphone"),
            ),
            SizedBox(height: 20.0),
            TextField(
              decoration: InputDecoration(
                  labelText: "Alamat",
                  hintMaxLines: 5,
                  border: OutlineInputBorder(
                      // borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(width: 2.0, color: Colors.grey)),
                  hintText: "Alamat"),
            ),
            SizedBox(height: 20.0),
            MaterialButton(
              padding: EdgeInsets.all(10.0),
              onPressed: (){},
              color: Colors.green,
              child: Text(
                "Update Profile",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
