import 'package:berasku/Controller/C_account.dart';
import 'package:berasku/Controller/C_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VAccount extends StatefulWidget {
  final String username;
  final String email;
  final String photoUrl;
  VAccount({this.username, this.email, this.photoUrl});
  @override
  _VAccountState createState() => _VAccountState();
}

class _VAccountState extends State<VAccount>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  // CONTROLLER
  CAccount cAccount = CAccount();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => CAuth(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Profile',
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Lobster',
              fontSize: 24,
            ),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              cAccount.getProfile(
                context,
                widget.username,
                widget.email,
                widget.photoUrl,
              ),
              Divider(),
              Container(
                height: 50.0,
                color: Colors.white,
                child: TabBar(
                  labelColor: Colors.red,
                  unselectedLabelColor: Colors.grey,
                  controller: _tabController,
                  tabs: <Widget>[
                    Text("My Transaction"),
                    Text("Pengeluaran"),
                  ],
                ),
              ),
              Divider(),
              Container(
                height: 500.0,
                color: Colors.white,
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    ListView(
                        physics: NeverScrollableScrollPhysics(),
                        children: cAccount.setDataTrans()),
                    ListView(
                        physics: NeverScrollableScrollPhysics(),
                        children: cAccount.setDataCashout())
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
