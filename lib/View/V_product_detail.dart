import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class VProductDetail extends StatelessWidget {
  final String tag;
  final String img;
  final String title;
  final String harga;

  VProductDetail({this.tag, this.img, this.title, this.harga});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                FontAwesomeIcons.angleLeft,
                color: Colors.red,
              ),
            ),
            pinned: true,
            expandedHeight: 200,
            flexibleSpace: FlexibleSpaceBar(
              background: Hero(
                tag: tag,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(this.img), fit: BoxFit.cover),
                  ),
                  height: 300.0,
                  // color: Colors.red,
                ),
              ),
            ),
          ),
          SliverFillRemaining(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(this.title,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0)),
                      FloatingActionButton(
                          backgroundColor: Colors.white,
                          child: Icon(Icons.favorite_border, color: Colors.red),
                          onPressed: () {})
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text("Rp ${this.harga}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                          fontSize: 18.0)),
                  SizedBox(
                    height: 26.0,
                  ),
                  Text("Deskripsi :",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.green,
                          fontSize: 18.0)),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                      "Beras A adalah beras dengan kualitas terbaik dengan berat 25 kg",
                      style: TextStyle(fontSize: 16.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  MaterialButton(
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {},
                    color: Colors.green,
                    child: Text("Tambah ke Keranjang",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 16.0)),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
