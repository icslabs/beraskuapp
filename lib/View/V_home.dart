import 'package:berasku/View/V_account.dart';
import 'package:berasku/View/V_beranda.dart';
import 'package:berasku/View/V_cart.dart';
import 'package:berasku/View/V_feed.dart';
import 'package:berasku/View/V_message.dart';
import 'package:berasku/View/V_wishlist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  final String name;
  final String email;
  final String photoUrl;

  Home({this.name, this.email, this.photoUrl});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  Future<SharedPreferences> sharePreference = SharedPreferences.getInstance();

  Future clearAll() async {
    final siap = await sharePreference;
    siap.clear();
  }

  int _selectedIndex = 0;

  List<BottomNavigationBarItem> _tabItems = [
    BottomNavigationBarItem(
      icon: Icon(FontAwesomeIcons.home),
      title: Text('Home'),
    ),
    BottomNavigationBarItem(
      icon: Icon(FontAwesomeIcons.newspaper),
      title: Text('Feed'),
    ),
    BottomNavigationBarItem(
      icon: Icon(FontAwesomeIcons.shoppingBasket),
      title: Text('Cart'),
    ),
    BottomNavigationBarItem(
      icon: Icon(FontAwesomeIcons.userAlt),
      title: Text('Profile'),
    ),
  ];

  _bottomNavBarIndex(int i) {
    setState(() {
      _selectedIndex = i;
    });
  }

  Widget searchBtn() {
    return SizedBox.expand(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        child: OutlineButton(
          child: Row(
            children: <Widget>[
              Icon(FontAwesomeIcons.search, size: 12),
              SizedBox(width: 12),
              Text('Cari Produk..')
            ],
          ),
          onPressed: () {
            MediaQueryData queryData;
            queryData = MediaQuery.of(context);
          },
          color: Colors.black54,
          borderSide: BorderSide(color: Colors.black54),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var _layoutPage = [
      Beranda(name: widget.name),
      VFeed(),
      VCart(),
      VAccount(
        username: widget.name,
        email: widget.email,
        photoUrl: widget.photoUrl,
      ),
    ];

    return Scaffold(
      appBar: _selectedIndex == 3
          ? null
          : AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              title: searchBtn(),
              actions: <Widget>[
                IconButton(
                  onPressed: () {
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (c) => VWishlist()));
                  },
                  icon: Icon(
                    FontAwesomeIcons.solidHeart,
                    color: Colors.black54,
                  ),
                ),
                IconButton(
                  onPressed: () {
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (c) => VMessage()));
                  },
                  icon: Icon(
                    FontAwesomeIcons.solidEnvelope,
                    color: Colors.black54,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    FontAwesomeIcons.solidBell,
                    color: Colors.black54,
                  ),
                )
              ],
            ),
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: _tabItems,
        currentIndex: _selectedIndex,
        onTap: _bottomNavBarIndex,
      ),
    );
  }
}
