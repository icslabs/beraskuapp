import 'package:berasku/Controller/C_beranda.dart';
import 'package:berasku/Model/M_beranda.dart';
import 'package:berasku/View/V_terlaris.dart';
import 'package:berasku/View/Widget/W_beranda.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Beranda extends StatefulWidget {
  final String name;

  Beranda({this.name});

  @override
  _BerandaState createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  CBeranda cBeranda = CBeranda();
  WBeranda wBeranda = WBeranda();
  MBeranda mBeranda = MBeranda();

  @override
  Widget build(BuildContext context) {
    // var mediaWidth = MediaQuery.of(context).size.width;
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Container(
      child: ListView(
        children: <Widget>[
          wBeranda.swiper(),
          SizedBox(height: 20.0),
          Container(
            padding: EdgeInsets.all(20.0),
            color: Colors.white,
            // height: 180.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Kategori",
                    style:
                        TextStyle(decorationStyle: TextDecorationStyle.solid)),
                SizedBox(height: 20.0),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: cBeranda.handleCategory())
              ],
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 30,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Terlaris"),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          CupertinoPageRoute(
                            builder: (context) => VTerlaris(),
                          ),
                        );
                      },
                      textColor: Theme.of(context).primaryColor,
                      child: Text("Lihat semua"),
                    )
                  ],
                ),
                SizedBox(height: 10.0),
                Container(
                  height: 170,
                  child: ListView.builder(
                    itemCount: 4,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => wBeranda.laris(
                      mBeranda.dataItemLaris[index]['title'],
                      mBeranda.dataItemLaris[index]['price'],
                      mBeranda.dataItemLaris[index]['tag'],
                      context,
                      mBeranda.dataItemLaris[index]['img'],
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Semua Produk",
                    style:
                        TextStyle(decorationStyle: TextDecorationStyle.solid)),
                SizedBox(height: 20.0),
                Container(
                  height: queryData.devicePixelRatio >= 2.0
                      ? (queryData.size.height / 4.2) * (3.2)
                      : (queryData.size.height / 4.2) * (3.7),
                  child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: 5,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 1 / 1.1,
                        crossAxisCount: 2,
                        mainAxisSpacing: 20,
                        crossAxisSpacing: 20),
                    itemBuilder: (context, index) => wBeranda.all(
                      mBeranda.dataItemAllProduk[index]['title'],
                      mBeranda.dataItemAllProduk[index]['price'],
                      mBeranda.dataItemAllProduk[index]['tag'],
                      context,
                      mBeranda.dataItemAllProduk[index]['img'],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
