import 'package:berasku/Controller/C_wishlist.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class VWishlist extends StatefulWidget {
  @override
  _VWishlistState createState() => _VWishlistState();
}

class _VWishlistState extends State<VWishlist> {
  CWishlist cWishlist = CWishlist();
  var myWishList;

  // @override
  // void initState() {
  //   super.initState();
  //   myWishList = cWishlist.handleItems(context);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          "My Wishlist",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(FontAwesomeIcons.angleLeft),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[cWishlist.handleItems(context)],
        ),
      ),
    );
  }
}
