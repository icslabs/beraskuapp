import 'package:berasku/Controller/C_feed.dart';
import 'package:flutter/material.dart';

class VFeed extends StatelessWidget {
  final CFedd cFedd = CFedd();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: ListView(
        children:  cFedd.handleDataItem()
      ),
    );
  }
}
