import 'package:berasku/View/V_home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'View/V_login.dart';

void main() {
  runApp(App());
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  Future<SharedPreferences> _shared = SharedPreferences.getInstance();

  String _name;
  String _email;
  String _img;

  @override
  void initState() {
    super.initState();
    _handleGet();
  }

  Future _handleGet() async {
    final sha = await _shared;
    _name = sha.getString("name");
    _email = sha.getString("email");
    _img = sha.getString("img");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    Map<int, Color> color = {
      50: Color(0xffea2c2c),
      100: Color(0xffea2c2c),
      200: Color(0xffea2c2c),
      300: Color(0xffea2c2c),
      400: Color(0xffea2c2c),
      500: Color(0xffea2c2c),
      600: Color(0xffea2c2c),
      700: Color(0xffea2c2c),
      800: Color(0xffea2c2c),
      900: Color(0xffea2c2c),
    };
    final MaterialColor _custColor = MaterialColor(0xffea2c2c, color);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );

    return MaterialApp(
      home: _email == null
          ? VLogin()
          : Home(
              name: _name,
              email: _email,
              photoUrl: _img,
            ),
      title: "Berasku",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Oxygen',
        primarySwatch: _custColor,
      ),
    );
  }
}
