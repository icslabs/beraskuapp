import 'package:flutter/material.dart';

class MBeranda {
  List<String> listImg = [
    'assets/img/slider1.jpg',
    'assets/img/slider2.jpg',
    'assets/img/slider3.jpg',
  ];

  var dataItems = [
    {
      "title": "Beras",
      "soon": "",
      "color": Colors.transparent,
    },
    {
      "title": "Galon",
      "soon": "Soon",
      "color": Colors.white70,
    },
    {
      "title": "Gas",
      "soon": "Soon",
      "color": Colors.white70,
    }
  ];

  var dataItemLaris = [
    {
      "title": "Beras Kepala",
      "price": "250000",
      "tag": "items1",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Ijo",
      "price": "150000",
      "tag": "items2",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items3",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items4",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items5",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
  ];

  var dataItemAllProduk = [
    {
      "title": "Beras Kepala",
      "price": "250000",
      "tag": "items4",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Ijo",
      "price": "150000",
      "tag": "items5",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items6",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items7",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items8",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items9",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items10",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items11",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
    {
      "title": "Beras Super",
      "price": "450000",
      "tag": "items12",
      "img":
          "https://tokoberasbagus.files.wordpress.com/2011/03/16-bmw-10_20.jpg"
    },
  ];
}
