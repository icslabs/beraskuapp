import 'package:berasku/Model/M_cart.dart';
import 'package:berasku/View/Widget/W_cart.dart';
import 'package:flutter/material.dart';

class CCart{
  MCart mCart = MCart();
  WCart wCart = WCart();

  int qty = 1;

  incrementQty(String title){
    for(int i = 0; i < mCart.dataItemCart.length; i++){
      if(title == mCart.dataItemCart[i]["title"]){
        print(title + " = " + mCart.dataItemCart[i]["title"]);
        qty++;
        print(qty);
      }
    }

    return qty;
  }

  decrementQty(String title){
    for(int i = 0; i < mCart.dataItemCart.length; i++){
      if(title == mCart.dataItemCart[i]["title"]){
        print(title + " = " + mCart.dataItemCart[i]["title"]);
        qty--;
      }
    }

    return qty;
  }

  handleDataItem(){
    List<Widget> _items = List();

    for(int i = 0; i < mCart.dataItemCart.length; i++){
      _items.add(
        wCart.itemCart(
          mCart.dataItemCart[i]["title"],
          mCart.dataItemCart[i]["price"],
          mCart.dataItemCart[i]["qty"],
        )
      );
    }

    return _items;
  }

  handleTotalItemsPrice(){
    double sum = 0;

    for(int i = 0; i < mCart.dataItemCart.length; i++){
      sum += mCart.dataItemCart[i]["price"];
    }

    return wCart.totalItemPrice(sum);
  }

  handleAlamat(){
    return wCart.alamat(mCart.alamat[0]["lang"], mCart.alamat[0]["lat"]);
  }

}