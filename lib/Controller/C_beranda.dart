import 'package:berasku/Model/M_beranda.dart';
import 'package:berasku/View/Widget/W_beranda.dart';
import 'package:flutter/material.dart';

class CBeranda {
  MBeranda mBeranda = MBeranda();
  WBeranda wBeranda = WBeranda();

  handleCategory() {
    List<Widget> items = List();

    for (int i = 0; i < mBeranda.dataItems.length; i++) {
      items.add(wBeranda.kategori(
        mBeranda.dataItems[i]["title"],
        mBeranda.dataItems[i]["soon"],
        mBeranda.dataItems[i]["color"],
      ));
    }

    return items;
  }

  handleLaris(BuildContext context) {
    List<Widget> items = List();

    for (int i = 0; i < mBeranda.dataItemLaris.length; i++) {
      items.add(wBeranda.laris(
        mBeranda.dataItemLaris[i]["title"],
        mBeranda.dataItemLaris[i]["price"],
        mBeranda.dataItemLaris[i]["tag"],
        context,
        mBeranda.dataItemLaris[i]["img"],
      ));
    }

    return items;
  }

  handleAllProduct(BuildContext context) {
    List<Widget> items = List();

    for (int i = 0; i < mBeranda.dataItemAllProduk.length; i++) {
      items.add(wBeranda.all(
        mBeranda.dataItemAllProduk[i]["title"],
        mBeranda.dataItemAllProduk[i]["price"].toString(),
        mBeranda.dataItemAllProduk[i]["tag"],
        context,
        mBeranda.dataItemAllProduk[i]['img'],
      ));
    }

    return items;
  }
}
