import 'package:berasku/View/V_home.dart';
import 'package:berasku/View/V_login.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:async';

class CAuth extends ChangeNotifier {
  final kFirebaseAuth = FirebaseAuth.instance;
  final kGoogleSignIn = GoogleSignIn();
  final kFirebaseAnalytics = FirebaseAnalytics();

  String _userName;
  String _email;
  FirebaseUser _user;

  String get userName => _userName;
  String get email => _email;
  FirebaseUser get fUser => _user;

  set fUser(FirebaseUser u) {
    _user = u;
    notifyListeners();
  }

  set userName(String u) {
    _userName = u;
    notifyListeners();
  }

  set email(String e) {
    _email = e;
    notifyListeners();
  }

  Future<FirebaseUser> googleSignin() async {
    FirebaseUser currentUser;
    try {
      final GoogleSignInAccount googleUser = await kGoogleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      final AuthResult user1 =
          await kFirebaseAuth.signInWithCredential(credential);
      assert(user1.user.email != null);
      assert(user1.user.displayName != null);
      assert(!user1.user.isAnonymous);
      assert(await user1.user.getIdToken() != null);
      currentUser = await kFirebaseAuth.currentUser();
      assert(user1.user.uid == currentUser.uid);
      print(currentUser);
      print("User Name : ${currentUser.displayName}");
      fUser = currentUser;
      userName = fUser.displayName;
      notifyListeners();
      email = fUser.email;
      notifyListeners();
      return currentUser;
    } catch (e) {
      print(e);
      return currentUser;
    }
  }

  String emailPref;

  Future<SharedPreferences> _sharedPref = SharedPreferences.getInstance();

  String _name;
  String _emailUser;

  get name => _name;
  get emailUser => _emailUser;

  set name(String n) {
    _name = n;
    notifyListeners();
  }

  set emailUser(String n) {
    _emailUser = n;
    notifyListeners();
  }

  Future _handleLogin(String username, String email, String img) async {
    final sha = await _sharedPref;
    sha.setString("name", username);
    sha.setString("email", email);
    sha.setString("img", img);
    return sha;
  }

  Future _handleGet() async {
    final sha = await _sharedPref;
    name = sha.getString("name");
    emailUser = sha.getString("email");
  }

  Future<void> logIn(BuildContext context) async {
    final gUser = await googleSignin();
    print('$gUser');
    print("UserName: $userName, Email: $email");

    if (gUser != null) {
      _handleLogin(userName, email, gUser.photoUrl.toString());
      _handleGet();
      await Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => Home(
                    name: userName,
                    email: email,
                    photoUrl: gUser.photoUrl.toString(),
                  )),
          (r) => false);
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Harus Login'),
              actions: <Widget>[
                RaisedButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text(
                    'Ok',
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            );
          });
    }
  }

  Future<void> logOut(BuildContext context) async {
    final sha = await _sharedPref;
    sha.clear();
    kFirebaseAuth.signOut();
    kGoogleSignIn.signOut();
    notifyListeners();
    print("UserName: $userName, Email: $email");
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => VLogin()));
    notifyListeners();
  }
}
