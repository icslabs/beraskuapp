import 'package:berasku/Model/M_account.dart';
import 'package:berasku/View/V_login.dart';
import 'package:berasku/View/Widget/W_account.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CAccount {
  MAccount dataAccount = MAccount();
  WAccount wAccount = WAccount();

  setDataTrans() {
    List<Widget> _listTrans = List();

    for (int i = 0; i < dataAccount.dataTrans.length; i++) {
      _listTrans.add(wAccount.trans(
          dataAccount.dataTrans[i]["code"],
          dataAccount.dataTrans[i]["price"],
          dataAccount.dataTrans[i]["status"]));
    }

    return _listTrans;
  }

  setDataCashout() {
    List<Widget> _listCahsout = List();

    for (int i = 0; i < dataAccount.dataCashout.length; i++) {
      _listCahsout.add(wAccount.cashout(
        dataAccount.dataCashout[i]["date"],
        dataAccount.dataCashout[i]["price"],
      ));
    }

    return _listCahsout;
  }

  getProfile(BuildContext context, String usr, String email, String photoUrl) {
    return wAccount.profile(context, email, usr, photoUrl);
  }

  Future<SharedPreferences> _sharedPref = SharedPreferences.getInstance();

  logout(BuildContext context) async {
    final sha = await _sharedPref;
    sha.clear();
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => VLogin()));
  }
}
