import 'package:berasku/Model/M_message.dart';
import 'package:berasku/View/Widget/W_message.dart';
import 'package:flutter/material.dart';

class CMessage{

  WMessage wMessage = WMessage();
  MMessage mMessage = MMessage();

  handleDataItems(){ 
    List<Widget> items = List();

    for(int i = 0 ; i < mMessage.items.length; i++){
      items.add(
        wMessage.item(
          mMessage.items[i]["author"],
          mMessage.items[i]["date"],
          mMessage.items[i]["desc"],
        )
      );
    }

    return items;
  }

}