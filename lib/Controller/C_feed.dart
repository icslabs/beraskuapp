import 'package:berasku/Model/M_feed.dart';
import 'package:berasku/View/Widget/W_feed.dart';
import 'package:flutter/material.dart';

class CFedd{

  MFedd mFedd = MFedd();
  WFeed wFeed = WFeed();

  handleDataItem(){
    List<Widget> items = List();

    for(int i = 0 ; i < mFedd.dataItemFeed.length; i ++){
      items.add(
        wFeed.item(
          mFedd.dataItemFeed[i]["author"],
          mFedd.dataItemFeed[i]["date"],
          mFedd.dataItemFeed[i]["desc"]
        )
      );
    }

    return items;
  }

}